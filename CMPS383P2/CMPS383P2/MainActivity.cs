﻿using Android.App;
using Android.Widget;
using Android.OS;
using RestSharp;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;
using RiotSharp;
using RiotSharp.StaticDataEndpoint;
using System.Configuration;
using System.Linq;
using Android.Content;

namespace CMPS383P2
{
    [Activity(Label = "CMPS383P2", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            EditText SummonerName = FindViewById<EditText>(Resource.Id.SummonerNameTxt);
            Button SearchBtn = FindViewById<Button>(Resource.Id.SearchBtn);

            SearchBtn.Click += (sender, e) =>
            {
               
                var summActivity = new Intent(this, typeof(SummonerActivity));
                summActivity.PutExtra("SummonerName", SummonerName.Text.ToString());
                StartActivity(summActivity);

            };

        }
        public class Summoner
        {
            public long id { get; set; }
            public string name { get; set; }
            public int profileIconId { get; set; }
            public long revisionDate { get; set; }
            public long summonerLevel { get; set; }
        }
    }
}

