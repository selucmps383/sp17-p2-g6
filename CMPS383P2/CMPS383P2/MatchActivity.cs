    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;
    using RiotSharp;
    using RiotSharp.GameEndpoint;

namespace CMPS383P2
{
    [Activity(Label = "MatchActivity")]
    public class MatchActivity : Activity
    {
        List<MatchInfo> matchList;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MatchHistoryLayout);

            var api = RiotApi.GetInstance("RGAPI-5b3804ed-eb7e-4f75-8e69-cee241d9b62b");
            var staticApi = StaticRiotApi.GetInstance("RGAPI-5b3804ed-eb7e-4f75-8e69-cee241d9b62b");
            ListView matchListView = FindViewById<ListView>(Resource.Id.MatchHistoryView);
            TextView Title = FindViewById<TextView>(Resource.Id.Title);
            

            //Put API call and stuff here
            string summonerName = Intent.GetStringExtra("SummonerName");
            Region region = (Region)Enum.Parse(typeof(Region), "na");
            var summ = api.GetSummoner(region, summonerName);
            string summonerId = Intent.GetStringExtra("SummonerId");
            var matches = api.GetRecentGames(region, summ.Id);
            
            Title.Text = summ.Name + "'s Match History";
            var position = 0;
            matchList = new List<MatchInfo>();
            foreach (var match in matches)
            {
                matchList.Add(new MatchInfo
                {
                    ChampionName = staticApi.GetChampion(region, matches[position].ChampionId).Name,
                    MatchDate = match.CreateDate.ToShortDateString(),
                    GameID = match.GameId.ToString(),
                    GameMode = match.GameMode.ToString(),
                    IPEarned = match.IpEarned.ToString(),
                    TeamID = DecideTeam(match.TeamId),
                    Win = DecideWin(match.Statistics.Win),
                    Item0 = match.Statistics.Item0.ToString(),
                    Item1 = match.Statistics.Item1.ToString(),
                    Item2 = match.Statistics.Item2.ToString(),
                    Item3 = match.Statistics.Item3.ToString(),
                    Item4 = match.Statistics.Item4.ToString(),
                    Item5 = match.Statistics.Item5.ToString(),
                    Item6 = match.Statistics.Item6.ToString(),
                    Gold = match.Statistics.GoldEarned.ToString(),
                    Minions = match.Statistics.MinionsKilled.ToString(),
                    Kills = match.Statistics.ChampionsKilled.ToString(),
                    Deaths = match.Statistics.NumDeaths.ToString(),
                    Assists = match.Statistics.Assists.ToString()
            });
                if (position < 10)
                {
                    position++;
                }
                else
                {
                    position = 9;
                }
            }

            //Populate ListView with our stuff
            ListAdapter listViewAdapter = new ListAdapter(this, matchList);
            matchListView.Adapter = listViewAdapter;
            matchListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
            {
                var MatchInfoActivity = new Intent(this, typeof(MatchInfoActivity));
                var t = matchList[e.Position];
                MatchInfoActivity.PutExtra("Champion", t.ChampionName.ToString());
                MatchInfoActivity.PutExtra("Result", t.Win.ToString());
                MatchInfoActivity.PutExtra("Team", t.TeamID.ToString());
                MatchInfoActivity.PutExtra("MatchDate", t.MatchDate.ToString());
                MatchInfoActivity.PutExtra("GameID", t.GameID.ToString());
                MatchInfoActivity.PutExtra("GameMode", t.GameMode.ToString());
                MatchInfoActivity.PutExtra("IPEarned", t.IPEarned.ToString());
                MatchInfoActivity.PutExtra("SummonerName", summonerName);
                MatchInfoActivity.PutExtra("Item0", t.Item0.ToString());
                MatchInfoActivity.PutExtra("Item1", t.Item1.ToString());
                MatchInfoActivity.PutExtra("Item2", t.Item2.ToString());
                MatchInfoActivity.PutExtra("Item3", t.Item3.ToString());
                MatchInfoActivity.PutExtra("Item4", t.Item4.ToString());
                MatchInfoActivity.PutExtra("Item5", t.Item5.ToString());
                MatchInfoActivity.PutExtra("Item6", t.Item6.ToString());
                MatchInfoActivity.PutExtra("Gold", t.Gold.ToString());
                MatchInfoActivity.PutExtra("Minions", t.Minions.ToString());
                MatchInfoActivity.PutExtra("Kills", t.Kills.ToString());
                MatchInfoActivity.PutExtra("Deaths", t.Deaths.ToString());
                MatchInfoActivity.PutExtra("Assists", t.Assists.ToString());
                StartActivity(MatchInfoActivity);
                //string alertText = "Champion: " + t.ChampionName + "\nResult: " + t.Win +
                //        "\nTeam: " + t.TeamID + "\nMatch Date: " + t.MatchDate + "\nGame ID: " +
                //        t.GameID + "\nGame Mode: " + t.GameMode + "\nIP Earned: " + t.IPEarned;
                //Toast.MakeText(this, alertText, ToastLength.Long).Show();
            };
    }

        

        public class ListAdapter : BaseAdapter<string>
        {
            List<MatchInfo> matches;
            Activity context;
            public ListAdapter(Activity context, List<MatchInfo> matches) : base()
            {
                this.context = context;
                this.matches = matches;
            }
            public override long GetItemId(int position)
            {
                return position;
            }
            public override string this[int position]
            {
                get { return matches[position].GameID; }
            }
            public override int Count
            {
                get { return matches.Count; }
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                View view = convertView; // re-use an existing view, if one is available
                if (view == null) // otherwise create a new one
                    view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
                if (matches[position].Win == "Victory")
                {
                    view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = "Win as " + matches[position].ChampionName + " on " + matches[position].GameMode;
                }else
                {
                    view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = "Loss as " + matches[position].ChampionName + " on " + matches[position].GameMode;
                }
                
                return view;
            }
        }
        
        private string DecideWin(bool win)
        {
            if (win == true)
            {
                return "Victory";
            } else
            {
                return "Defeat";
            }
        }

        private string DecideTeam(int teamId)
        {
            if (teamId == 100)
            {
                return "Blue";
            } else
            {
                return "Purple";
            }
        }
    }
    public class MatchInfo
    {
        public string ChampionName { get; set; }
        public string MatchDate { get; set; }
        public string GameID { get; set; }
        public string GameMode { get; set; }
        public string IPEarned { get; set; }
        public string TeamID { get; set; }
        public string Win { get; set; }
        public string Item0 { get; set; }
        public string Item1 { get; set; }
        public string Item2 { get; set; }
        public string Item3 { get; set; }
        public string Item4 { get; set; }
        public string Item5 { get; set; }
        public string Item6 { get; set; }
        public string Gold { get; set; }
        public string Minions { get; set; }
        public string Kills { get; set; }
        public string Deaths { get; set; }
        public string Assists { get;set; }
}
}