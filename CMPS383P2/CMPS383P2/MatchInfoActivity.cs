using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RiotSharp;
using System.Net;

namespace CMPS383P2
{
    [Activity(Label = "MatchInfoActivity")]
    public class MatchInfoActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MatchInfoLayout);
            TextView ChampnameText = FindViewById<TextView>(Resource.Id.ChampText);
            TextView Coins = FindViewById<TextView>(Resource.Id.GoldCount);
            TextView Minions = FindViewById<TextView>(Resource.Id.MinionCount);
            TextView Score = FindViewById<TextView>(Resource.Id.KDACount);
            TextView DateWas = FindViewById<TextView>(Resource.Id.DatePlayed);
            ImageView ChampIcon = FindViewById<ImageView>(Resource.Id.ChampPic);
            ImageView Item0Icon = FindViewById<ImageView>(Resource.Id.item0Pic);
            ImageView Item1Icon = FindViewById<ImageView>(Resource.Id.item1Pic);
            ImageView Item2Icon = FindViewById<ImageView>(Resource.Id.item2Pic);
            ImageView Item3Icon = FindViewById<ImageView>(Resource.Id.item3Pic);
            ImageView Item4Icon = FindViewById<ImageView>(Resource.Id.item4Pic);
            ImageView Item5Icon = FindViewById<ImageView>(Resource.Id.item5Pic);
            ImageView Item6Icon = FindViewById<ImageView>(Resource.Id.item6Pic);
            ImageView GoldCount = FindViewById<ImageView>(Resource.Id.GoldIcon);
            ImageView MinionCount = FindViewById<ImageView>(Resource.Id.MinionsIcon);
            ImageView ScoreTrack = FindViewById<ImageView>(Resource.Id.ScoreIcon);

            var api = RiotApi.GetInstance("RGAPI-5b3804ed-eb7e-4f75-8e69-cee241d9b62b");
            var summName = Intent.GetStringExtra("SummonerName");
            var GetChampion = Intent.GetStringExtra("Champion");
            var Champion = GetChampion.Replace(" ", "");
            long GameID = Convert.ToInt64(Intent.GetStringExtra("GameID"));
            var Result = Intent.GetStringExtra("Result");
            var Team = Intent.GetStringExtra("Team");
            var MatchDate = Intent.GetStringExtra("MatchDate");
            var GameMode = Intent.GetStringExtra("GameMode");
            var IPEarned = Intent.GetStringExtra("IPEarned");
            var Item0 = Intent.GetStringExtra("Item0");
            var Item1 = Intent.GetStringExtra("Item1");
            var Item2 = Intent.GetStringExtra("Item2");
            var Item3 = Intent.GetStringExtra("Item3");
            var Item4 = Intent.GetStringExtra("Item4");
            var Item5 = Intent.GetStringExtra("Item5");
            var Item6 = Intent.GetStringExtra("Item6");
            var gold = Intent.GetStringExtra("Gold");
            var minions = Intent.GetStringExtra("Minions");
            var kills = Intent.GetStringExtra("Kills");
            var deaths = Intent.GetStringExtra("Deaths");
            var assists = Intent.GetStringExtra("Assists");

            //Region region = (Region)Enum.Parse(typeof(Region), "na");
           
            // Create your application here
            string ChampName = GetChampion;
            ChampnameText.Text = ChampName;
            var champImage = GetImageBitmapFromUrl("champion/", Champion);
            var item0Image = GetImageBitmapFromUrl("item/", Item0);
            var item1Image = GetImageBitmapFromUrl("item/", Item1);
            var item2Image = GetImageBitmapFromUrl("item/", Item2);
            var item3Image = GetImageBitmapFromUrl("item/", Item3);
            var item4Image = GetImageBitmapFromUrl("item/", Item4);
            var item5Image = GetImageBitmapFromUrl("item/", Item5);
            var item6Image = GetImageBitmapFromUrl("item/", Item6);
            var goldIcon = GetImageBitmapFromUrl("ui/", "gold");
            var minionIcon = GetImageBitmapFromUrl("ui/", "minion");
            var scoreIcon = GetImageBitmapFromUrl("ui/", "score");
            ChampIcon.SetImageBitmap(champImage);
                Item0Icon.SetImageBitmap(item0Image);
                Item1Icon.SetImageBitmap(item1Image);
                Item2Icon.SetImageBitmap(item2Image);
                Item3Icon.SetImageBitmap(item3Image);
                Item4Icon.SetImageBitmap(item4Image);
                Item5Icon.SetImageBitmap(item5Image);
                Item6Icon.SetImageBitmap(item6Image);
            GoldCount.SetImageBitmap(goldIcon);
            MinionCount.SetImageBitmap(minionIcon);
            ScoreTrack.SetImageBitmap(scoreIcon);
            Coins.Text = "Gold Earned: " + gold + "\nIP Earned: " + IPEarned;
            Minions.Text = "Minions Killed: " + minions;
            Score.Text = "Kills/Deaths/Assists: " + kills + "/" + deaths + "/" + assists;
            DateWas.Text = "Match played on " + MatchDate;

            
        }

        private Android.Graphics.Bitmap GetImageBitmapFromUrl(string type, string url)
        {
            Android.Graphics.Bitmap imageBitmap = null;
            string urlStart = "http://ddragon.leagueoflegends.com/cdn/6.13.1/img/";
            string urlEnd = urlStart + type + url + ".png";

            using (var webClient = new System.Net.WebClient())
            {
                if (url == "0")
                {
                    urlEnd = "http://vignette2.wikia.nocookie.net/leagueoflegends/images/d/d5/Item.png/revision/latest?cb=20100922194922";
                    
                }else if (url == "gold")
                {
                    urlEnd = "http://ddragon.leagueoflegends.com/cdn/5.5.1/img/ui/gold.png";

                }else if (url == "minion")
                {
                    urlEnd = "http://ddragon.leagueoflegends.com/cdn/5.5.1/img/ui/minion.png";

                }else if (url == "score")
                {
                    urlEnd = "http://ddragon.leagueoflegends.com/cdn/5.5.1/img/ui/score.png";
                }
                
                    var imageBytes = webClient.DownloadData(urlEnd);

                    if (imageBytes != null && imageBytes.Length > 0)
                    {
                        imageBitmap = Android.Graphics.BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    }

                    return imageBitmap;
                
            }

        }
    }
}