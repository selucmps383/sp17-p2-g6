using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RiotSharp;
using RiotSharp.SummonerEndpoint;

namespace CMPS383P2
{
        [Activity(Label = "RuneActivity")]
        public class RuneActivity : Activity
        {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.RuneLayout);
            var api = RiotApi.GetInstance("RGAPI-5b3804ed-eb7e-4f75-8e69-cee241d9b62b");
            var staticApi = StaticRiotApi.GetInstance("RGAPI-5b3804ed-eb7e-4f75-8e69-cee241d9b62b");
            Region region = (Region)Enum.Parse(typeof(Region), "na");
            long summonerId = Convert.ToInt64(Intent.GetStringExtra("SummonerId"));
            ListView runeListView = FindViewById<ListView>(Resource.Id.RuneListView);

            List<long> summList = new List<long>();
            summList.Add(summonerId);

            //Put API call and stuff here
            //List<RunePage> summonerRunePages;
            //api.GetRunePages(region, summList).TryGetValue(summonerId, out summonerRunePages);
            //var runePages = summonerRunePages;
            var runePages = api.GetRunePages(region, summList).First().Value;
            RuneAdapter runeViewAdapter = new RuneAdapter(this, runePages);
            runeListView.Adapter = runeViewAdapter;
            runeListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
            {
                var t = runePages[e.Position];
                string alertText = "";
                foreach(var slot in t.Slots)
                {
                    string runeName = staticApi.GetRune(region, slot.RuneId).Name;
                    alertText += runeName + "\n";
                }
                Toast.MakeText(this, alertText, ToastLength.Long).Show();
            };
        }
        public class RuneAdapter : BaseAdapter<string>
        {
            List<RunePage> runePages;
            Activity context;
            public RuneAdapter(Activity context, List<RunePage> runePages) : base()
            {
                this.context = context;
                this.runePages = runePages;
            }
            public override long GetItemId(int position)
            {
                return position;
            }
            public override string this[int position]
            {
                get { return runePages[position].Id.ToString(); }
            }
            public override int Count
            {
                get { return runePages.Count; }
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                View view = convertView; // re-use an existing view, if one is available
                if (view == null) // otherwise create a new one
                    view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
                view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = runePages[position].Name;
                return view;
            }
        }
    }
}