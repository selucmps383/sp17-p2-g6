using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RiotSharp;

namespace CMPS383P2
{
    [Activity(Label = "SummonerActivity")]
    public class SummonerActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.SummonerLayout);
            var api = RiotApi.GetInstance("RGAPI-5b3804ed-eb7e-4f75-8e69-cee241d9b62b");
            string summonerName = Intent.GetStringExtra("SummonerName");
            Region region = (Region)Enum.Parse(typeof(Region), "na");
            var summ = api.GetSummoner(region, summonerName);
            var summId = summ.Id;
            Console.Out.WriteLine("Your ID Is:");
            Console.Out.WriteLine(summId);

            Button MatchHistoryButton = FindViewById<Button>(Resource.Id.MatchHistoryButton);
            //Button RunesButton = FindViewById<Button>(Resource.Id.RunesButton);
            TextView SummonerNameDisplay = FindViewById<TextView>(Resource.Id.SumName);
            TextView SummonerLevel = FindViewById<TextView>(Resource.Id.SumLevel);
            ImageView SummonerIcon = FindViewById<ImageView>(Resource.Id.SumIcon);

            var imageBitmap = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/6.13.1/img/profileicon/" + summ.ProfileIconId + ".png");
            SummonerIcon.SetImageBitmap(imageBitmap);
            SummonerNameDisplay.Text = summ.Name;
            SummonerLevel.Text = "Level "+summ.Level.ToString();

            MatchHistoryButton.Click += (sender, e) =>
            {

                var matchActivity = new Intent(this, typeof(MatchActivity));
                matchActivity.PutExtra("SummonerName", summ.Name);
                StartActivity(matchActivity);

            };
            //RunesButton.Click += (sender, e) =>
            //{
            //    var runesActivity = new Intent(this, typeof(RuneActivity));
            //    runesActivity.PutExtra("SummonerId", summId);
            //    StartActivity(runesActivity);
            //};
        }

        private Android.Graphics.Bitmap GetImageBitmapFromUrl(string url)
{
            Android.Graphics.Bitmap imageBitmap = null;

     using (var webClient = new System.Net.WebClient())
     {
          var imageBytes = webClient.DownloadData(url);
          if (imageBytes != null && imageBytes.Length > 0)
          {
               imageBitmap = Android.Graphics.BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
          }
     }

     return imageBitmap;
}

    }
}